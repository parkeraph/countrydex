import React, {useEffect, useState} from 'react';

const CountryDetail = ({country}) => {
    const languageList = country.languages.map(thisLang => {
        return(
            <li>{thisLang.name}</li>
        )
    })
    
    return(
        <div className="countryDetailed"> 
            <h3>{country.name}</h3>
            <div className="details">
                <img src={country.flag} width="75px"/>
                <p>Region: {country.subregion}</p>
                <p>Population: {country.population}</p>
                <p>languages: </p>
                <ul>{languageList}</ul>
            </div>
        </div>
    )
}

const Country = (props) => {
    const data = props.data;
    const [countryDataState, setCountryDataState] = useState(data);

    const reloadData = () => {
        let countryList; 
        let escape = false;

        if(data.length > 100){
            countryList = <p>Too many results, please narrow query.</p>
            escape = true;
        }

        if(data.length === 0 || data.status === 404){
            countryList = <p>Nothing found for search query</p>
            escape = true;
        }

        if(!escape){
            if(data.length <= 5){
                countryList = data.map(thisCountry => {
                    return(
                        <CountryDetail country={thisCountry} key={thisCountry.alpha2Code} />
                    )
                }) 
            }else{
                countryList = data.map(thisCountry => {
                    return(
                        <div key={thisCountry.alpha2Code} className="country">
                            <h3>{thisCountry.name}</h3>
                        </div>
                    )
                })
            }
        }

        setCountryDataState(countryList)
        console.log("state: ", countryList)
    }

    useEffect(reloadData, [data])

    return(
        <div id="countryContainer">
           <h2>Search Results</h2>
            {countryDataState}
        </div>    
    )
}

export default Country;