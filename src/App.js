import React, { useState, useEffect } from 'react'
import ReactDOM from 'react';
import axios from 'axios'
import './App.css';
import Country from './Country';

const App = () => {
	const [searchInputState, setSearchInputState] = useState("");
	const [queryState, setQueryState] = useState([]);

	const handleInputChange = (event) => {
		console.log(event.target.value);
		setSearchInputState(event.target.value);

		axios.get("https://restcountries.eu/rest/v2/name/"+searchInputState).then(result => {
			console.log("Query responce: ", result.data);
			setQueryState(result.data);
		}).catch( err => {
			console.log(err.response.data);
			setQueryState(err.response.data);
		});
	}


	return (
    <div id="formContainer">
		
		<input id="countryInput" placeholder="Enter Country Name" onChange={handleInputChange} />
		
		<Country  data={queryState} />
	</div>
  );
}

export default App;
